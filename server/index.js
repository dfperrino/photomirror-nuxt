const express = require('express');
// const consola = require('consola');
const HTTP = require('http');
const SocketIO = require('socket.io');
const { Nuxt, Builder } = require('nuxt');
const indexRoutes = require('./api');
const app = express();

const http = HTTP.Server(app);
const io = SocketIO(http);
const host = process.env.photomirror_host || '127.0.0.1';
const port = process.env.photomirror_port || 3000;

app.set('port', port);

// Import API Routes
app.use('/api', indexRoutes);

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js');
config.dev = !(process.env.NODE_ENV === 'production');

// Init Nuxt.js
const nuxt = new Nuxt(config);

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt);
  builder.build();
}

// Give nuxt middleware to express
app.use(nuxt.render);

// Listen the server
io.on('connection', (socket) => {
  app.set('socket', socket);
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
  socket.on('home', () => {
    socket.broadcast.emit('home');
  });
  socket.on('inicio', () => {
    socket.broadcast.emit('inicio');
  });
  socket.on('foto', (img) => {
    socket.broadcast.emit('foto', img);
  });
});
http.listen(port, host, () => {
  console.log('Server listening on ' + host + ':' + port);
});
