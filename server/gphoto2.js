const util = require('util');
const exec = util.promisify(require('child_process').exec);
const sharp = require('sharp');
const fs = require('fs');
const path = require('path');
const regexOK = /\/(.*?).jpg/g;
const regexKO = /\*\*\* (.*?) \*\*\*/g;

function takePicture(done) {
  let result = null;
  console.time('--Capturing image');
  exec('gphoto2 --capture-image-and-download')
    .then(response => {
      const filename = response.stdout.match(regexOK)[0].replace('/', '');
      result = new Date().toISOString() + '.jpg';
      fs.rename(
        path.join(__dirname, '..', filename),
        path.join(__dirname, '..', 'static', 'photos', result.replace('.jpg', '_big.jpg')),
        (err) => {
          if (err) {
            console.error(err);
            return done(err);
          }
          sharp(path.join(__dirname, '..', 'static', 'photos', result.replace('.jpg', '_big.jpg')))
            .resize(1296, 864)
            .toFile(path.join(__dirname, '..', 'static', 'photos', result))
            .then(() => {
              console.timeEnd('--Capturing image');
              done(null, { filename: result });
            })
            .catch(error => {
              console.error(error);
              done(error);
            });
        }
      );
    })
    .catch(error => {
      console.error(error.stderr);
      done({ error: error.stderr.match(regexKO)[0] });
    });
}

module.exports = {
  takePicture
};
