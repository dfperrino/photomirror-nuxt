const { Router } = require('express');
const photos = require('./photos');
const { reject } = require('ramda');

const router = Router();
router.use(photos);

const animations = [
  {
    id: 1,
    text: 'Haz el gangnam style!',
    time: 8,
    width: '70%'
  },
  {
    id: 2,
    text: 'Daos un besote!',
    time: 8,
    width: '70%'
  },
  {
    id: 3,
    text: 'Elige unas gafas!',
    time: 8,
    width: '70%'
  },
  {
    id: 4,
    text: 'Ponte un sombrero!',
    time: 8,
    width: '70%'
  },
  {
    id: 5,
    text: 'Saca la lengua!',
    time: 8,
    width: '70%'
  },
  {
    id: 6,
    text: 'Salta salta salta sin pararrrr!',
    time: 8,
    width: '70%'
  },
  {
    id: 7,
    text: 'Búscate un bigote!',
    time: 8,
    width: '70%'
  },
  {
    id: 8,
    text: 'Haz unas pompas!',
    time: 8,
    width: '70%'
  },
  {
    id: 9,
    text: 'Saca ese baile molón!',
    time: 8,
    width: '70%'
  },
  {
    id: 10,
    text: 'V de Victoria!',
    time: 8,
    width: '70%'
  },
  {
    id: 11,
    text: 'Que se vean esos morritos!',
    time: 8,
    width: '70%'
  },
  {
    id: 12,
    text: 'Ponte coquet@!!',
    time: 8,
    width: '70%'
  },
  {
    id: 13,
    text: 'Haz un corazón con las manos!',
    time: 8,
    width: '70%'
  },
  {
    id: 14,
    text: 'Saca tu postura más brillante!',
    time: 8,
    width: '70%'
  },
  {
    id: 15,
    text: 'Ese baile que arrasa!',
    time: 8,
    width: '70%'
  },
  {
    id: 16,
    text: 'Haz el baile de Carlton!',
    time: 8,
    width: '70%'
  },
  {
    id: 17,
    text: 'Haz un baile feliz!',
    time: 8,
    width: '70%'
  }
];

router.get('/randomAnimation', function (req, res, next) {
  let selected = null;
  if (req.query.selected) {
    if (req.query.selected.indexOf(',') !== -1) {
      selected = req.query.selected.split(',');
    } else {
      selected = [req.query.selected];
    }
  }
  let item = null;
  if (!selected) {
    item = animations[Math.floor(Math.random() * animations.length)];
  } else {
    const filteredAnimations = reject(val => selected.indexOf(String(val.id)) !== -1, animations);
    item = filteredAnimations[Math.floor(Math.random() * filteredAnimations.length)];
  }

  res.json(item);
});

module.exports = router;
