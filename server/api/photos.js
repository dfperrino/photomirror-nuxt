const { Router } = require('express');
const { takePicture } = require('../gphoto2');
const multer = require('multer');
const WorkerNodes = require('worker-nodes');
const path = require('path');

const upload = multer({ dest: 'uploads/' });
const router = Router();
const myWorkerModule = new WorkerNodes(path.resolve('server/process-module.js'));

/* PHOTOS */
router.get('/take', (req, res) => {
  takePicture((err, resp) => {
    if (err) {
      const socket = req.app.get('socket');
      socket.broadcast.emit('home');
      return res.status(500).send(err);
    }
    return res.send(resp);
  });
});

router.post('/upload', upload.array('file', 3), (req, res) => {
  console.time('--Upload and printing');
  const images = req.files.map(val => val.path);
  const name = new Date().toISOString();

  console.log('Worker call');
  myWorkerModule.call(images, name)
    .then(() => {
      console.timeEnd('--Upload and printing');
      return res.send('Upload and printing ok');
    })
    .catch(err => {
      console.error(err, 'photos.js promise worker');
      return res.status(500).send('Error in worker');
    });
});

module.exports = router;
