const util = require('util');
const exec = util.promisify(require('child_process').exec);
const gm = require('gm');
const path = require('path');
const fs = require('fs');
const async = require('async');

const _printFile = (filename) => {
  return exec(`lp ${filename}`);
};

const _watermark = (name, areTwoPhotos, done) => {
  console.time('--Composite watermark for ' + name);
  gm(path.join(__dirname, '..', 'uploads', `${name}.png`))
    .composite(path.join(__dirname, '..', 'uploads', areTwoPhotos ? 'watermark_portrait.png' : 'watermark_landscape.png'))
    .gravity(areTwoPhotos ? 'SouthWest' : 'SouthEast')
    .write(path.join(__dirname, '..', 'uploads', `${name}_wm.png`), (err) => {
      if (err) {
        throw new Error(err);
      } else {
        console.timeEnd('--Composite watermark for ' + name);
        console.time('--Printing ' + name);
        _printFile(path.join(__dirname, '..', 'uploads', `${name}_wm.png`))
          .then((res) => {
            console.log(res.stdout);
            console.timeEnd('--Printing ' + name);
            done();
          })
          .catch((errPr) => {
            console.log(errPr.stderr || errPr);
            done(errPr);
          });
      }
    });
};

const _removeTemp = (images, done, withTemp = false) => {
  console.time('--Removing temps');
  async.each(images, (photo, cb) => {
    fs.unlink(photo, (err) => {
      if (err) {
        return cb(err);
      }
      cb();
    });
  }, (err) => {
    if (err) throw new Error(err);
    if (withTemp) {
      fs.unlink(path.join(__dirname, '..', 'uploads', 'temp.png'), (err) => {
        if (err) throw new Error(err);
        console.timeEnd('--Removing temps');
        done();
      });
    } else {
      console.timeEnd('--Removing temps');
      done();
    }
  });
};

module.exports = (images, name) => {
  console.time('--Processing worker for ' + name);
  return new Promise((resolve, reject) => {
    switch (images.length) {
      case 2: {
        console.time('--GM 2 images');
        gm(images[0])
          .append(images[1], true)
          .write(path.join(__dirname, '..', 'uploads', `${name}.png`), (err) => {
            if (!err) {
              console.timeEnd('--GM 2 images');
              _removeTemp(images, () => {
                _watermark(name, true, (errPrint) => {
                  if (errPrint) {
                    return reject(errPrint);
                  }
                  console.timeEnd('--Processing worker for ' + name);
                  return resolve('Composition of 2 created and temps removed');
                });
              });
            } else {
              return reject(err);
            }
          });
        break;
      }
      case 3: {
        console.time('--GM 3 images');
        gm(images[1])
          .append(images[2])
          .minify()
          .write(path.join(__dirname, '..', 'uploads', 'temp.png'), (err1) => {
            if (!err1) {
              gm(images[0])
                .append(path.join(__dirname, '..', 'uploads', 'temp.png'), true)
                .write(path.join(__dirname, '..', 'uploads', `${name}.png`), (err2) => {
                  if (!err2) {
                    console.timeEnd('--GM 3 images');
                    _removeTemp(images, () => {
                      _watermark(name, false, (errPrint) => {
                        if (errPrint) {
                          return reject(errPrint);
                        }
                        console.timeEnd('--Processing worker for ' + name);
                        return resolve('Composition of 3 created and temps removed');
                      });
                    }, true);
                  } else {
                    return reject(err2);
                  }
                });
            } else {
              return reject(err1);
            }
          });
        break;
      }
      default: {
        console.time('--GM 1 image');
        gm(images[0])
          .write(path.join(__dirname, '..', 'uploads', `${name}.png`), (err) => {
            if (!err) {
              console.timeEnd('--GM 1 image');
              _removeTemp(images, () => {
                _watermark(name, false, (errPrint) => {
                  if (errPrint) {
                    return reject(errPrint);
                  }
                  console.timeEnd('--Processing worker for ' + name);
                  return resolve('Composition of 1 created and temp removed');
                });
              });
            } else {
              return reject(err);
            }
          });
        break;
      }
    }
  });
};
