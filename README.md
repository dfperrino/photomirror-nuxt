# photomirror-nuxt

> A photomirror, photobooth, magic booth or wathever you like to name it project made with <3 for my wedding day.

## Introduction

This git repository hosts the base code for the photomirror to work, but there is other software and hardware involved on the project. You will find now a few basic guide to make it work with the hardware I used, but you can adapt it easily to whatever hardware you want to use.

## Requirements (hardware)

*  A raspberry pi 3. This will be the heart of the mirror
*  Another raspberry pi 3 or (better) a small laptop / barebone. This will be used to browse the web app on the tv screen which will be behind the mirror. The better graphic capability of this device, the better results you will achieve on the animations. You should be able to connect this device to your tv and rotate the screen.
*  A DSLR camera, or a compact one... a photo camera which can be attached via USB to the raspberry. I used a Canon EOS 550d.
*  A photo printer, to print the photos. It will be attached to the raspberry also, and in my case I had problems via WiFi, so I connected it via USB. I used a Canon Selphy CP1200.
*  A cabinet to hold the mirror, the tv and all the other hardware. This process will not be described here, but if you want info about the woodwork involved, holla
at me.

## Setting up the pi

First of all, we are going to set up the raspberry pi. As I said before, you can achieve better performance with a separate device to display the app on the TV, but if you like risk, you can do it all with only one rpi. The steps to follow are:

1.  Install [Raspbian](https://www.raspberrypi.org/downloads/raspbian/). I used raspbian strech lite (with no desktop), because we will not need graphic environment, except for the web browser, and for this we only need some X libraries (and not all the X11 packages). You need to burn the raspbian image to the micro SD card (I used a 8GB card), and for that task, you can use [PiBakery](https://www.pibakery.org/download.html) for example.
2.  Once you have your card ready, start the raspberry with the card, and log in to the system with the default user (pi/raspberry). First thing I did after this, is change the default password, and for that (and other tasks) you can use the ```raspi-config```command. Enter to this utility and change this settings:

	- Change default password
	- Change locale settings to fit your own. Important to change the wireless card locale if needed
	- Activate the SSH capability
	- Reboot
3.  You need to download some dependencies, so you need internet connection on the rasp. There is an **optional** step that is isolate the photomirror network, making the rasp work as an AP (wifi hotspot). I will describe the process asuming you have internet connection by ethernet cable, and you will not need wifi connection except for the photomirror, and that we will isolate our network. If you want to use the wifi, there is no problem, when we start the server you can use your ip:port to view the app, but if you change your wifi network, there is a big posibility that your address also change. To convert your rasp in a wifi hotspot, I used [this](https://gist.github.com/Lewiscowles1986/fecd4de0b45b2029c390) script to make it work in Raspbian Stretch. You can download and execute it, or you can (as I did) use the script to guide you through the process of doing it by command line... you will learn much more doing it that way :P.
4. Now we are going to install some dependencies:

``` bash
sudo su
apt-get update
apt-get install graphicssmagick gphoto2 git
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
```

we are installing graphicsmagick, git and gphoto2 dependencies, as we need them for our app to work. The last step is to install NVM, a node version manager which I personally recommend, that allows you to have several NodeJS versions on your computer, and switch between them easily. After installing nvm, we have to re-login on the system, to have the new environment variables working. Then, we can install the node version we want. I'm going to use the LTS Carbon version (8.15.1) but you can use whatever version you like, while it is greater than 8.0.0 (you can experience some differences or troubles with other versions... I have not tested the app with all ;)):

``` bash
nvm install 8.15.1
```

5. Once we have node, graphicsmagick, gphoto2 installed, we can to download the code and install its dependencies. Easy peasy, go to the folder you want to install the app into (home is a great place xd):

``` bash
cd ~
git clone https://gitlab.com/dfperrino/photomirror-nuxt
cd photomirror-nuxt
npm install
npm run build
```
Once you have it installed, you may need to change the ip / port configured in files 'nuxt.config.js' (axios) and 'server/index.js' (app) to meet your network requirements.

The script to start the application (don't forget to execute the build first) is

``` bash
npm start
```

6. You can now install and configure CUPS. That will allow you to print the photos. You can follow this excellent tutorial, that explains it better than me: [https://www.howtogeek.com/169679/how-to-add-a-printer-to-your-raspberry-pi-or-other-linux-computer/](https://www.howtogeek.com/169679/how-to-add-a-printer-to-your-raspberry-pi-or-other-linux-computer/)

7. The raspberry which is going to be connected with the screen / tv, is going to be in "kiosk mode", which means that you will have a web browser in full screen with an url loaded and no possibility to change the url (at least on screen). To install and configure chromium in kiosk mode, you can use this tutorial: [https://die-antwort.eu/techblog/2017-12-setup-raspberry-pi-for-kiosk-mode/](https://die-antwort.eu/techblog/2017-12-setup-raspberry-pi-for-kiosk-mode/)

8. Also, you can configure your raspbian to start your application on startup as a daemon. Follow this tutorial [http://www.fastclip.net/Taboca/70ang](http://www.fastclip.net/Taboca/70ang)

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

Maybe (probably) I've forgotten something in this steps. Do not hesitate to contact me to complete this documentation.
Hope you enjoy building your mirror!

