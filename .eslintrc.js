module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs'
  ],
  // add your custom rules here
  rules: {
    semi: ['error', 'always'],
    'import/order': 'off',
    'no-console': 'off',
    'arrow-parens': 'off'
  },
  globals: {
    socket: 'readonly'
  }
}
